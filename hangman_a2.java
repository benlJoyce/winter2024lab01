import java.util.Scanner;

public class hangman_a2 {

    public static void main(String[] args) {
        Scanner word = new Scanner(System.in);
        System.out.println("Choose your word: ");

        String wordInput = word.nextLine();
        wordInput = wordInput.toUpperCase();
		
		System.out.print("\033c");
        runGame(wordInput);
    }

    public static int isLetterInWord(String word, char c) {
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == c) {
                return i; 
            }
        }
        return -1;
    }

    public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
        String playerResult = "You found: ";

        if (letter0) {
            playerResult += toUpperCase(word.charAt(0)) + " ";
        } else {
            playerResult += "_ ";
        }
        if (letter1) {
            playerResult += toUpperCase(word.charAt(1)) + " ";
        } else {
            playerResult += "_ ";
        }
        if (letter2) {
            playerResult += toUpperCase(word.charAt(2)) + " ";
        } else {
            playerResult += "_ ";
        }
        if (letter3) {
            playerResult += toUpperCase(word.charAt(3)) + " ";
        } else {
            playerResult += "_ ";
        }
        System.out.println(playerResult);
    }

    public static void runGame(String word) {
        boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;
        int missCount = 0;

        while (missCount < 6 && !(letter0 && letter1 && letter2 && letter3)) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nPlace your guess: ");

            String guessStr = scanner.nextLine().toUpperCase();
            char guess = guessStr.charAt(0);

            int result = isLetterInWord(word, guess);
            if (result == -1) {
                missCount++;
				System.out.println("Uh oh your miss count rised! You have " + missCount + " " + "misses D:");
            } else if (result == 0 && !letter0) {
                letter0 = true;
				System.out.println("You got it! ");
            } else if (result == 1 && !letter1) {
                letter1 = true;
				System.out.println("You're good at this!");
            } else if (result == 2 && !letter2) {
                letter2 = true;
				System.out.println("Good guess :D");
            } else if (result == 3 && !letter3) {
                letter3 = true;
				System.out.println("Good job !!");
            }

            printWork(word, letter0, letter1, letter2, letter3);
        }

        if (missCount >= 6) {
            System.out.println("\nLOSERRRR Better luck next time :P The word was: " + word);
        } 
		else {
            System.out.println("\nAw you got me there... You win! Wanna play again?");
        }
    }
}
